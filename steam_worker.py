# -*- coding: utf-8 -*-
import httplib2
import urllib2
import json
import re

import json_worker

h = httplib2.Http()


# Function to perform request for remote url
def perform_remote_request(url):
    result = json.loads(h.request(url, 'GET')[1])
    return result


# Function to get current item price on steam market
def get_item_price(item_name, item_skin, item_exterior):
    url = "http://steamcommunity.com/market/priceoverview/?country=RU&currency=5&appid=730&market_hash_name=%s%s%s&language=english" % (urllib2.quote(item_name)+"%20%7C%20", urllib2.quote(item_skin)+"%20%28", urllib2.quote(item_exterior)+"%29")
    result = perform_remote_request(url)
    print url
    print item_name
    # find all characters in the string that are numeric.
    print result
    if "lowest_price" in result:
        m = re.search(r'\d+', result['lowest_price'])
        # retrieve numeric string
        price = m.group()
    elif "median_price" in result:
        m = re.search(r'\d+', result['median_price'])
        # retrieve numeric string
        price = m.group()
    return int(price)


def get_item_icons(item_name, item_skin, item_exterior):
    # http://steamcommunity.com/market/listings/730/{ITEM_NAME}%20%7C%20{SKIN_NAME}%20%28{USAGE_THINGY Ex. Well-Worn}%29
    # http://steamcommunity.com/market/listings/730/AWP%20%7C%20Asiimov%20%28Field-Tested%29/render?start=0&count=1&currency=3&language=english&format=json
    url = "http://steamcommunity.com/market/listings/730/%s%s%s/render?start=0&count=1&currency=3&language=english&format=json" % (urllib2.quote(item_name)+"%20%7C%20", urllib2.quote(item_skin)+"%20%28", urllib2.quote(item_exterior)+"%29")
    # Perform request
    result = perform_remote_request(url)
    # Parse JSON
    icon_small = "https://steamcommunity-a.akamaihd.net/economy/image/" + result['assets']['730']['2'].values()[0]['icon_url']
    icon_big = "https://steamcommunity-a.akamaihd.net/economy/image/" + result['assets']['730']['2'].values()[0]['icon_url_large']
    # Return object
    return (icon_small, icon_big)


def get_steam_user(steam_id):
    url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%d" % (json_worker.get_api_key(), steam_id)

    print url

    result = perform_remote_request(url)
    user = result['response']['players'][0]

    return user
