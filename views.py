from flask import Flask, request, jsonify, abort, g
from models import Base, User, Transaction, Case, Weapon
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from random import shuffle
import time, random
import json_worker, worker, steam_worker
from flask.ext.httpauth import HTTPBasicAuth
from functools import update_wrapper

from redis import Redis

redis = Redis()

'Auth'
auth = HTTPBasicAuth()

''' Handling database '''
engine = create_engine(json_worker.load_db_path())

Base.metadata.bind = engine

users_DBSession = sessionmaker(bind=engine)
session = users_DBSession()

app = Flask(__name__)

''' VIEW RATES HANDLING '''


class RateLimit(object):
    expiration_window = 10

    def __init__(self, key_prefix, limit, per, send_x_headers):
        self.reset = (int(time.time()) // per) * per + per
        self.key = key_prefix + str(self.reset)
        self.limit = limit
        self.per = per
        self.send_x_headers = send_x_headers
        p = redis.pipeline()
        p.incr(self.key)
        p.expireat(self.key, self.reset + self.expiration_window)
        self.current = min(p.execute()[0], limit)

    remaining = property(lambda x: x.limit - x.current)
    over_limit = property(lambda x: x.current >= x.limit)


def get_view_rate_limit():
    return getattr(g, '_view_rate_limit', None)


def on_over_limit(limit):
    return jsonify({'data': 'You hit the rate limit', 'error': '429'}), 429


def ratelimit(limit, per=300, send_x_headers=True,
              over_limit=on_over_limit,
              scope_func=lambda: request.remote_addr,
              key_func=lambda: request.endpoint):
    def decorator(f):
        def rate_limited(*args, **kwargs):
            key = 'rate-limit/%s/%s/' % (key_func(), scope_func())
            rlimit = RateLimit(key, limit, per, send_x_headers)
            g._view_rate_limit = rlimit
            if over_limit is not None and rlimit.over_limit:
                return over_limit(rlimit)
            return f(*args, **kwargs)

        return update_wrapper(rate_limited, f)

    return decorator


@app.after_request
def inject_x_rate_headers(response):
    limit = get_view_rate_limit()
    if limit and limit.send_x_headers:
        h = response.headers
        h.add('X-RateLimit-Remaining', str(limit.remaining))
        h.add('X-RateLimit-Limit', str(limit.limit))
        h.add('X-RateLimit-Reset', str(limit.reset))
    return response


''' USER API ENDPOINTS '''


@app.route("/items-for-case/<int:case_id>", methods=['GET'])
@auth.login_required
@ratelimit(limit=json_worker.get_rate_limit(), per=json_worker.get_rate_limit_reset_time())
def items_for_case(case_id):
    # Find a case entry under specified id
    case = session.query(Case).filter_by(id=case_id).first()
    # If there is such a case
    if case:
        weapons = get_weapons_for_case(case)
        if weapons:
            return jsonify(weapons=[i.serialize for i in weapons])

    else:
        return jsonify({
            "data": {
                "result": "error",
                "description": "There is no such a case"
            }
        })


@app.route("/items-for-slider/<int:case_id>", methods=['GET'])
@auth.login_required
@ratelimit(limit=json_worker.get_rate_limit(), per=json_worker.get_rate_limit_reset_time())
def items_for_slider(case_id):
    # Find a case entry under specified id
    case = session.query(Case).filter_by(id=case_id).first()
    # If there is such a case
    if case:
        weapons = get_weapons_for_case(case)
        weapons + get_extra_weapons_for_case(case, 10)
        shuffle(weapons)
        if weapons:
            return jsonify(message=return_message(200), weapons=[i.serialize for i in weapons])

    else:
        return jsonify({
            "data": {
                "result": "error",
                "description": "There is no such a case"
            }
        })


@app.route("/open-case/<int:id>,<int:case_open_bonus>", methods=['GET'])
@auth.login_required
@ratelimit(limit=json_worker.get_rate_limit(), per=json_worker.get_rate_limit_reset_time())
def start_open_case(id, case_open_bonus):
    # Find a case entry under specified id
    case = session.query(Case).filter_by(id=id).first()
    # If there is such a case
    if case:
        # If user can open the case
        if check_user_availability_to_open(case):
            # Get all weapons for the case
            weapons = get_weapons_for_case(case)
            # If weapons list is not empty:
            if weapons:
                # Generate a weapon to open
                weapon_to_open, profit, loss = worker.generate_weapon_to_open(case, weapons)
                # Set offset for client slider for the weapon
                client_offset = worker.calculate_client_slider_offset(weapons, weapon_to_open)
                # Create transaction
                transaction = Transaction(client_id=g.user.id, status="ok", item_id=weapon_to_open.id,
                                          timestamp=worker.get_timestamp(), offset_hash=client_offset)
                # Save transaction
                session.add(transaction)
                session.commit()
                # Charge user
                if withdraw_weapon_price_from_user_balance(case, case_open_bonus) is not None:
                    return jsonify(message=return_message(200), offset=client_offset,
                                   transaction_id=transaction.id,
                                   weapon=weapon_to_open.serialize)
                else:
                    return return_json_message(4011)
            # Otherwise
            else:
                # Return empty object
                return jsonify()
        else:
            return return_json_message(600)

    else:
        return return_json_message(404)


@app.route("/sell-item/<int:transaction_id>", methods=['GET'])
@auth.login_required
@ratelimit(limit=json_worker.get_rate_limit(), per=json_worker.get_rate_limit_reset_time())
def sell_item(transaction_id):
    # Find transaction
    transaction = session.query(Transaction).filter_by(id=transaction_id).first()
    # If transaction exists
    if transaction:
        # If transaction belongs to requesting user
        if transaction.client_id == g.user.id:
            # Find item listed in transaction
            item = session.query(Weapon).filter_by(id=transaction.item_id).first()
            # Deposit item's price
            deposit_item_price_on_user_account(item)
            remove_item_from_user_account(item)
            return return_json_message(2002)
        else:
            return return_json_message(401)
    else:
        return return_json_message(404)


def remove_item_from_user_account(item):
    # Find user
    user = session.query(User).filter_by(id=g.user.id).first()
    # If user exists
    if user:
        # # Get all items of a user
        # items = [i.id for i in user.items.split(",")]
        # # If item is in items
        # if item in items:
        #     items.remove(item)
        #
        # session.commit()
        # return True

        if str(item.id) in user.items:
            user.items.replace(",%d" % item.id,"")
            session.commit()
            return True
        else:
            return False
    else:
        return False


@app.route("/get-item/<int:transaction_id>", methods=['GET'])
@auth.login_required
@ratelimit(limit=json_worker.get_rate_limit(), per=json_worker.get_rate_limit_reset_time())
def add_item(transaction_id):
    transaction = session.query(Transaction).filter_by(id=transaction_id).first()
    if transaction:
        if transaction.client_id == g.user.id:
            weapon = session.query(Weapon).filter_by(id=transaction.item_id).first()
            if weapon:
                # Add item to user
                user = session.query(User).filter_by(id=g.user.id).first()
                if user.items is not None or user.items is not "":
                    user.items = str(user.items) + ",%d" % weapon.id
                else:
                    user.items = "%d" % weapon.id
                # Check if first element is ","
                if user.items[0] == ",":
                    user.items = user.items[1:]
                # delete transaction
                session.delete(transaction)
                session.commit()
                return return_json_message(2001)
            else:
                return return_json_message(404)
        else:
            return return_json_message(401)
    else:
        return return_json_message(404)


''' ADMIN API ENDPOINTS '''

''' Weapons '''


@app.route("/test-case/<int:id>,<int:times>")
@auth.login_required
def test_case(id, times):
    if is_user_admin():
        opened_weapons = []
        total_profit = 0
        average_client_loss = 0
        for i in xrange(0, times):
            case = session.query(Case).filter_by(id=id).first()
            weapon_to_open, profit, loss = worker.generate_weapon_to_open(case, get_weapons_for_case(case))
            opened_weapons.append(weapon_to_open)
            total_profit += profit
            average_client_loss += loss
        average_client_loss /= times
        return jsonify(weapons=[i.quality for i in opened_weapons], profit=total_profit, loss=average_client_loss)
    else:
        return return_json_message(401)


@app.route('/update-weapons-price', methods=['POST'])
@auth.login_required
def update_weapons_price():
    if is_user_admin():
        weapons = session.query(Weapon).all()
        for weapon in weapons:
            weapon.calculate_price()
            session.commit()
            time.sleep(0.2)
        return_json_message(401)
    else:
        return return_json_message(401)


@app.route('/update-weapons-icons', methods=['POST'])
@auth.login_required
def update_weapons_icons():
    if is_user_admin():
        weapons = session.query(Weapon).all()
        for weapon in weapons:
            weapon.get_icons()
            session.commit()
            time.sleep(0.02)
        return_json_message(401)
    else:
        return return_json_message(401)


@app.route("/weapons", methods=['POST', 'GET'])
@auth.login_required
def weapons():
    if is_user_admin():
        if request.method == 'GET':
            weapons = session.query(Weapon).all()
            if cases:
                return jsonify(weapons=[i.serialize for i in weapons])
            else:
                return jsonify()
        elif request.method == 'POST':
            name = request.args.get('name')
            skin = request.args.get('skin')
            quality = request.args.get('quality')
            exterior = request.args.get('exterior')
            case_id = request.args.get('case_id')

            if check_any_filed_is_empty([name, skin, quality, case_id, exterior]):
                # Abort
                abort(400)
            # If measurement already exists
            if session.query(Case).filter_by(name=name).first() is not None:
                # Abort
                abort(400)

            weapon = Weapon(name=name, skin=skin, quality=quality, exterior=exterior, case_id=case_id,
                            )
            # Get icons for weapon
            weapon.get_icons()
            # Get price for weapon
            weapon.calculate_price()
            # Add entry to db
            session.add(weapon)
            # Save changer
            session.commit()
            # Return results
            return jsonify(weapon=weapon.serialize)
    else:
        return return_json_message(401)


@app.route("/weapons/<int:id>", methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def weapon(id):
    if is_user_admin():
        weapon = session.query(Weapon).filter_by(id=id).first()
        if request.method == 'GET':
            if case:
                return jsonify(weapon=weapon.serialize)
            else:
                return jsonify()

        elif request.method == 'DELETE':
            session.delete(weapon)
            session.commit()
            return return_json_message(200)

        elif request.method == 'PUT':
            args = ["id", "name", "skin", "quality", "exterior", "case_id"]
            rules = [
                {
                    "argument": "price",
                    "value": "update",
                    "function": steam_worker.get_item_price(weapon.name, weapon.skin, weapon.exterior)
                }
            ]
            parse_put(weapon, args, rules)
            # Update db
            session.commit()
            # Return updated object as json
            return jsonify(weapon=weapon.serialize)

    else:
        return return_json_message(401)


''' CASES '''


@app.route("/update-case-weapon-ids/<int:id>", methods=['GET'])
@auth.login_required
def update_case_weapon_ids(id):
    if is_user_admin():
        new_case_ids = ""
        # Find all weapons with specified case id
        weapons = session.query(Weapon).filter_by(case_id=id).all()
        shuffle(weapons)
        if weapons:
            for weapon in weapons:
                new_case_ids += "%d," % weapon.id
            case = session.query(Case).filter_by(id=id).first()
            case.weapons_ids = new_case_ids
            session.commit()
            return jsonify({"Message": "Case has been updated"})
        else:
            return return_json_message("error")
    else:
        return return_json_message("error")


@app.route("/cases", methods=['POST', 'GET'])
@auth.login_required
def cases():
    if is_user_admin():
        if request.method == 'GET':
            cases = session.query(Case).all()
            if cases:
                return jsonify(cases=[i.serialize for i in cases])
            else:
                return jsonify()
        elif request.method == 'POST':
            name = request.args.get('name')
            price = request.args.get('price')
            icon = request.args.get('icon')

            if check_any_filed_is_empty([name, price, icon]):
                # Abort
                abort(400)
            # If measurement already exists
            if session.query(Case).filter_by(name=name).first() is not None:
                # Abort
                abort(400)

            case = Case(name=name, price=price, icon=icon)
            # Add entry to db
            session.add(case)
            # Save changer
            session.commit()
            # Return results
            return jsonify(
                    case=case.serialize
            )
    else:
        return return_json_message(401)


@app.route("/cases/<int:id>", methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def case(id):
    case = session.query(Case).filter_by(id=id).first()
    if is_user_admin():
        if request.method == 'GET':
            if case:
                return jsonify(message=return_message(200),case=case.serialize)
            else:
                return jsonify()

        elif request.method == 'DELETE':
            session.delete(case)
            session.commit()
            return jsonify({'Message': 'Case has been deleted'})

        elif request.method == 'PUT':
            name = request.args.get('name')
            weapons_ids = request.args.get('weapons_ids')
            quality = request.args.get('quality')
            price = request.args.get('price')
            icon = request.args.get('icon')

            # Get case args
            if name:
                case.name = name
            if weapons_ids:
                case.weapons_ids = weapons_ids
            if quality:
                case.quality = quality
            if price:
                case.price = price
            if icon:
                case.icon = icon

            # Update db
            session.commit()
            # Return updated object as json
            return jsonify(case=case.serialize)

    else:
        if request.method == 'GET':
            if case:
                return jsonify(message=return_message(200), case=case.serialize)
            else:
                return jsonify()

        else:
            return_json_message(401)

''' USERS '''


@app.route("/token", methods=['GET'])
@auth.login_required
def get_auth_token():
    log("New token generation request!\nUsername: %s" % (g.user.username))

    # Genetate token
    token = g.user.generate_token()
    return jsonify({'token': token.decode('ascii')})


@app.route('/users', methods=['POST'])
def users():
    if request.method == 'POST':
        username = request.args.get('username')
        name = request.args.get('name')
        surname = request.args.get('surname')
        password = request.args.get('password')
        email = request.args.get('email')
        picture = request.args.get('picture')
        tel = request.args.get('tel')

        # If some data is missing
        if check_any_filed_is_empty([username, name, surname, password, email, picture, tel]):
            # Abort
            abort(400)
        # If user already exists
        if session.query(User).filter_by(username=username).first() is not None:
            # Abort
            abort(400)

        # Otherwise everything is ok, create user

        user = User(username=username, email=email, picture=picture, name=name, surname=surname, balance=0, bonus=0,
                    tel=tel)
        # Hash password
        user.hash_password(password)
        user.picture = steam_worker.get_steam_user(user.id)['avatarfull']
        # Add entry to db
        session.add(user)
        # Save changer
        session.commit()
        # Return results
        return jsonify({
            'username': username
        })


@app.route("/users", methods=["GET"])
@auth.login_required
def users_admin():
    if is_user_admin():
        users = session.query(User).all()
        if users:
            return jsonify(users=[i.serialize for i in users])
        else:
            return jsonify()
    else:
        return jsonify()


@app.route("/users/<int:id>", methods=["GET", 'DELETE', 'PUT'])
@auth.login_required
def user(id):
    user = session.query(User).filter_by(id=id).one()
    if request.method == 'GET':
        if user:
            return jsonify(user=user.serialize)
        else:
            return jsonify()
    elif request.method == 'DELETE':
        session.delete(user)
        session.commit()
        return jsonify({'Message': 'User has been deleted'})

    elif request.method == 'PUT':
        args = ["id", "username", "name", "surname", "password", "email", "picture", "tel", "balance", "bonus", "referral_friends_id", "items"]
        rules = [
            {
                "argument": "picture",
                "value": "update",
                "function": steam_worker.get_steam_user(g.user.id)["avatarfull"]
            },
            {
                "argument": "items",
                "value": "empty",
                "function": ""
            }
        ]
        parse_put(user, args, rules)

        session.commit()

        return jsonify(user=user.serialize)

''' Validate password for authentication '''


@auth.verify_password
def verify_password(username_or_token, password):
    # Get uset id via username of specified token
    user_id = User.verify_auth_token(username_or_token)
    # If it is a token
    if user_id:
        # Get user by id
        user = session.query(User).filter_by(id=user_id).one()

    # If it is a username
    else:
        user = session.query(User).filter_by(username=username_or_token).first()
        # If there is no such a user of password is incorrect
        if not user or not user.verify_password(password):
            # Return false
            return False
    # Store user in flask global
    g.user = user
    # Password is ok
    return True


''' Debug functions '''


def log(message):
    message = "-" * 30 + "\n" + worker.get_timestamp() + "\n" + message + "\n" + "-" * 30
    print message

    text_file = open("Output.txt", "a")
    text_file.write(message + "\n")
    text_file.close()


# Function to check if current user is in admin list
def is_user_admin():
    if g.user.username in set(json_worker.get_admin_names()):
        return True
    return False


# Method to check if all provided fields are not empty
# array(string) -> bool
def check_any_filed_is_empty(fields):
    for filed in fields:
        if filed is None:
            print "Null"
            return True
    return False


# Function to get all weapon object stored in a case object
def get_weapons_for_case(case):
    weapons = []
    for weapon_id in case.weapons_ids.split(","):
        weapon = session.query(Weapon).filter_by(id=weapon_id).first()
        if weapon:
            weapons.append(weapon)
    if weapons:
        # for i in xrange(0, 20):
        #     weapons.append(weapons[random.randint(0, len(weapons) - 1)])
        return weapons
    else:
        return None


# Function to add bad quality weapons to slider
def get_extra_weapons_for_case(case, number):
    output_weapons = []
    weapons = session.query(Weapon).filter_by(quality='Mil-spec').all()
    for i in xrange(0, number):
        random_index = random.randint(0, len(weapons) - 1)
        output_weapons.append(weapons[random_index])
    if output_weapons:
        return output_weapons
    else:
        return None


# Function to check if users balance is enough to buy a case
def check_user_availability_to_open(case):
    user = g.user
    if user.balance < case.price:
        return False

    return True


# Function to return message for code as JSON
def return_json_message(code):
    return jsonify({
        "message": json_worker.get_error_description(code)
    })


# Function to return message for code as object
def return_message(code):
    return json_worker.get_error_description(code)


# Function to withdraw weapon price from users account
def withdraw_weapon_price_from_user_balance(case, case_open_bonus):
    if case_open_bonus not in [0, 20, 40, 60]:
        return None

    user = session.query(User).filter_by(id=g.user.id).first()
    if user:
        user.balance -= (case.price + case_open_bonus)
        session.commit()
        return user.balance
    else:
        return None


# Function to read data of class data_class from DB under data_key=data_value query
def find_data(data_class, data_key, data_value):
    if data_key is not None and data_value is not None:
        return session.query(data_class).filter_by(data_key=data_value)


# Function to pars argument for POST request
def parse_post(object, args):
    for key in args:
        value = request.args.get(key)
        if value:
            setattr(object, key, value)


# Function to pars arguments and apply rules where needed for PUT request
def parse_put(object, args, rules):
    # 1. Get all keys of rules
    rules_keys = [i['argument'] for i in rules]
    # 2. Foreach args as key
    for key in args:

        # 3. Get url value for key
        value = request.args.get(key)
        # 4. If key should be under specific rules
        if key in rules_keys:

            # 5. Get index of this key
            index = rules_keys.index(key)
            # 6. If value of url argument is identical to rule one
            if value == rules[index]['value']:
                # 7. Set attribute key of object to value of function of rule
                setattr(object, key, rules[index]['function'])
        else:
            if value:
                setattr(object, key, value)


# Function to deposit item's price onto user's account
def deposit_item_price_on_user_account(item):
    # Find user
    user = session.query(User).filter_by(id=g.user.id).first()
    # If user exists
    if user:
        # Deposit
        user.balance += item.median_price
        return True
    else:
        return False

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
