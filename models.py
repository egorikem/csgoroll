from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, DDL
from passlib.apps import custom_app_context as pwd_context


import json_worker, worker, steam_worker

from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)

Base = declarative_base()

# Secret key for token generation
secret_key = worker.get_secret_key()

''' Classes '''


class Transaction(Base):
    __tablename__ = 'transaction'
    id = Column(Integer, primary_key=True)
    client_id = Column(Integer)
    status = Column(String(64))
    item_id = Column(Integer)
    timestamp = Column(String(100))
    offset_hash = Column(String(64))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'client_id': self.client_id,
            'offset_hash': self.offset_hash,
            'status': self.status,
            'item_id': self.item_id,
            'timestamp': self.timestamp,
        }

    # Method to generate token based on secret key and user id
    # int -> string
    def generate_offset_token(self):
        s = Serializer(secret_key)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_offset_token(token):
        s = Serializer(secret_key)
        try:
            data = s.loads(token)
        except SignatureExpired:
            # Invalid token: expired
            return None
        except BadSignature:
            # Invalid token: signature
            return None

        # Return user id
        return data['id']


class Case(Base):
    __tablename__ = 'case'
    id = Column(Integer, primary_key=True)
    name = Column(String(64))
    weapons_ids = Column(String(1000))
    quality = Column(String(64))
    price = Column(Integer)
    icon = Column(String(1000))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'weapons_ids': self.weapons_ids,
            'quality': self.quality,
            'price': self.price,
            'icon': self.icon,
        }


class Weapon(Base):
    __tablename__ = 'weapon'
    id = Column(Integer, primary_key=True)
    name = Column(String(64))
    skin = Column(String(64))
    quality = Column(String(64))
    exterior = Column(String(64))
    case_id = Column(Integer)
    drop_chance = Column(Integer)
    icon_small = Column(String(100))
    icon_big = Column(String(100))
    median_price = Column(Integer)

    # Method to get weapon price
    def calculate_price(self):
        self.median_price = steam_worker.get_item_price(self.name, self.skin, self.exterior)
        print "weapon price: %d" % self.median_price

    def get_icons(self):
        (icon_small, icon_big) = steam_worker.get_item_icons(self.name, self.skin, self.exterior)
        self.icon_small = icon_small
        self.icon_big = icon_big

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'skin': self.skin,
            'quality': self.quality,
            "exterior": self.exterior,
            'case_id': self.case_id,
            'drop_chance': self.drop_chance,
            'icon_small': self.icon_small,
            'icon_big': self.icon_big,
            'median_price': self.median_price
        }

    @property
    def client_serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'skin': self.skin,
            'quality': self.quality,
            "exterior": self.exterior,
            'icon_small': self.icon_small,
            'icon_big': self.icon_big,
            'median_price': self.icon
        }


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String(64))
    surname = Column(String(64))
    username = Column(String(64))
    password = Column(String(32))
    email = Column(String(64))
    picture = Column(String(64))
    tel = Column(String(64))
    balance = Column(Integer)
    opened_cases = Column(String(1000))
    items = Column(String(1000))
    sent_items = Column(String(1000))
    bonus = Column(Integer)
    referral_friends_id = Column(String(1000))


    # Function to encrypt password via hash function
    # string ->
    def hash_password(self, password):
        self.password = pwd_context.encrypt(password)

    # Function to verify provided hash of password with stored value
    # string -> bool
    def verify_password(self, password):
        return pwd_context.verify(password, self.password)

    # Method to generate token based on secret key and user id
    # int -> string
    def generate_token(self, expiration=1):
        expiration = expiration * 24 * 60 * 60 * 1000
        s = Serializer(secret_key, expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(secret_key)
        try:
            data = s.loads(token)
        except SignatureExpired:
            # Invalid token: expired
            return None
        except BadSignature:
            # Invalid token: signature
            return None

        # Return user id
        return data['id']

    @property
    def serialize(self):
        return {
            'id': self.id,
            'username': self.username,
            'password': self.password,
            'name': self.name,
            'surname': self.surname,
            'email': self.email,
            'picture': self.picture,
            "tel": self.tel,
            "balance": self.balance,
            "opened_cases": self.opened_cases,
            "items": self.items,
            "sent_items": self.sent_items,
            "bonus": self.bonus,
            "referral_friends_id": self.referral_friends_id
        }

    @property
    def client_serialize(self):
        return {
            # should we really do it?
        }

engine = create_engine(json_worker.load_db_path())
Base.metadata.create_all(engine)


def add_column(engine, table, column):
  table_name = table
  column_name = column.compile(dialect=engine.dialect)
  column_type = column.type.compile(engine.dialect)
  engine.execute('ALTER TABLE %s ADD COLUMN %s %s' % (table_name, column_name, column_type))

# column = Column('offset_hash', String(64))
# add_column = DDL('ALTER TABLE transaction ADD COLUMN offset_hash VARCHAR(64) AFTER timestamp')
# engine.execute(add_column)