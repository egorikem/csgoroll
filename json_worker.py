import json


def load_data(filename='settings.json'):
    try:
        with open(filename) as data_file:
            data = json.load(data_file)
            return data
    except IOError:
        return None


def update_data(key, value, filename='settings.json'):
    try:
        # Open file and grab data
        with open(filename, "r") as jsonFile:
            data = json.load(jsonFile)
        # Update data
        data[key] = value
        # Save data to file
        with open(filename, "w") as jsonFile:
            jsonFile.write(json.dumps(data))

    except IOError:
        return None


def load_data_for_key(key, filename="settings.json"):
    data = load_data(filename)
    if data:
        return data[key]
    else:
        return None


def get_secret_key():
    return load_data_for_key("secret_key")

def update_key(key):
    update_data("secret_key", key)


def load_db_path():
    return load_data_for_key("db_path")


def load_admin_username():
    return load_data_for_key("admin_username")


def get_admin_names():
    return load_data_for_key("admins")


# Function to get quality coefficients for weapon generation
def get_quality_coefficients():
    return load_data_for_key("quality", "coefficients.json")


# Function to get exterior coefficients for weapon generation
def get_exterior_coefficients():
    return load_data_for_key("exterior", "coefficients.json")


# Function to get weapon icon size from ui
def get_weapon_item_size():
    return load_data_for_key("weapon-item-size")


# Function to get rate limit
def get_rate_limit():
    return load_data_for_key("rate_limit")


# Function to get rate limit reset time
def get_rate_limit_reset_time():
    return load_data_for_key("rate_limit_reset_time")


# Function to get slider weapon item margin
def get_weapon_item_margin():
    return load_data_for_key("weapon_item_margin")


# Function to get weapon item icon size
def get_weapon_icon_size():
    return load_data_for_key("weapon-icon-size")


# Function to load error code description
def get_error_description(code):
    return load_data_for_key(str(code), "errors.json")


# Function to load api key
def get_api_key():
    return load_data_for_key("api_key")