import random, string
import json_worker
import time, datetime

from steam_worker import get_item_price


def generate_token():
    json_worker.update_key(''.join(random.choice(string.ascii_uppercase + string.digits) for x in xrange(32)))


def get_secret_key():
    if json_worker.load_data_for_key("secret_key") == "":
        generate_token()

    return json_worker.load_data_for_key("secret_key")


# Function to generate weapon to open
def generate_weapon_to_open(case, weapons_list):
    # Ratings list
    ratings = []
    # The goal is to not to let weapon to cover price of a case for random times
    # rand(0,number_of_times) times should drop item that costs item_cover_price_of_case times less than the case
    # The left 1 time should drop item that is more expensive than the case
    case_price = case.price
    # 1. Foreach weapons to calculate rating for each one
    for weapon in weapons_list:
        ratings.append(calculate_weapon_quality_rating(weapon))
    # 2. Find maximal rating (aka most relevant weapon)
    max_rating = max(ratings)
    # 3. Find weapon in weapons_list by its rating index in ratings list
    weapon_to_open = weapons_list[ratings.index(max_rating)]
    # 4. Update weapon price
    # 5. Calculate profit
    profit = case_price - weapon_to_open.median_price
    # 6. Calculate client's loss
    loss = int(float(profit) / float(case_price) * 100)
    return weapon_to_open, profit, loss


# Function to calculate weapon quality rating
def calculate_weapon_quality_rating(weapon):
    print weapon.quality
    print weapon.exterior
    rating = random.randint(1, 1000) * get_numeric_weapon_quality(weapon) + random.randint(1, 10) * get_numeric_weapon_quality(weapon) - random.randint(1, 10) * weapon.median_price
    return rating


def get_weapon_for_rating_to_pass(rating_to_pass, wapons_list):
    weapons_passed = []
    for weapon in wapons_list:
        if get_numeric_weapon_quality(weapon) + get_numeric_weapon_exterior > rating_to_pass:
            weapons_passed.append(weapon)


# Function to get numeric representation of weapon quality
def get_numeric_weapon_quality(weapon):
    # Get coefficients object from JSON file
    coefficients = json_worker.get_quality_coefficients()
    # Return corresponding coefficients based
    return {
        'Consumer Grade': coefficients['Consumer Grade'],
        'Industrial Grade': coefficients['Industrial Grade'],
        'Mil-spec': coefficients['Mil-spec'],
        'Restricted': coefficients['Restricted'],
        'Classified': coefficients['Classified'],
        'Covert': coefficients['Covert'],
        'Exceedingly Rare': coefficients['Exceedingly Rare'],
    }.get(weapon.quality, None)


# Function to get numeric representation of weapon quality
def get_numeric_weapon_exterior(weapon):
    coefficients = json_worker.get_exterior_coefficients()
    return {
        'Battle-Scarred': coefficients['Battle-Scarred'],
        'Well-Worn': coefficients['Well-Worn'],
        'Field-Tested': coefficients['Field-Tested'],
        'Minimal Wear': coefficients['Minimal Wear'],
        'Factory New': coefficients['Factory New'],
    }.get(weapon.exterior, None)


# Function
def calculate_client_slider_offset(weapons, weapon):
    index = weapons.index(weapon)
    item_size = json_worker.get_weapon_item_size()
    if index == 0:
        return item_size / 2
    else:
        return index * (item_size + json_worker.get_weapon_item_margin()) + random.randint(0,item_size-20)


# Function to generate timestamp
def get_timestamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
